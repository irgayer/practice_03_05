﻿using CefSharp;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Practice_03_05
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string DEFAULT_PAGE = "google.com";

        public MainWindow()
        {
            InitializeComponent();
            browser.Address = DEFAULT_PAGE;
            browserTabs.Items.Add(new TabItem
            {
                Header = DEFAULT_PAGE,
                Name = ""
            });
            browserTabs.SelectedItem = browserTabs.Items[0];
            urlBox.Text = getSelectedTabHeader();
        }

        private void addTabClick(object sender, RoutedEventArgs e)
        {
            browserTabs.Items.Add(new TabItem
            {
                Header = DEFAULT_PAGE,
                Name = "" 
            });
            browser.Address = DEFAULT_PAGE;
            browserTabs.SelectedItem = browserTabs.Items[browserTabs.Items.Count - 1];
            urlBox.Text = getSelectedTabHeader();
        }

        private void browserTabsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            browser.Address = getSelectedTabHeader();
            urlBox.Text = browser.Address;
        }

        private string getSelectedTabHeader()
        {
            TabItem currentTab = browserTabs.SelectedItem as TabItem;
            return (string)currentTab.Header;
        }


        private void urlBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                browser.Address = urlBox.Text;
                TabItem currentTab = browserTabs.SelectedItem as TabItem;
                currentTab.Header = urlBox.Text;
            }
        }

        private void deleteTabClick(object sender, RoutedEventArgs e)
        {
            if(browserTabs.Items.Count > 1)
            {
                object tabToDelete = browserTabs.SelectedItem;
                if (browserTabs.Items[0].Equals(tabToDelete))
                {
                    browserTabs.SelectedItem = browserTabs.Items[1];
                }
                else
                {
                    browserTabs.SelectedItem = browserTabs.Items[0];
                }
                browserTabs.Items.Remove(tabToDelete);
            }
        }

        private void browserBackClick(object sender, RoutedEventArgs e)
        {
            if (browser.CanGoBack)
            {
                browser.Back();
            }
        }

        private void browserForthClick(object sender, RoutedEventArgs e)
        {
            if (browser.CanGoForward)
            {
                browser.Forward();
            }
        }
    }
}
